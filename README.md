# TASK MANAGER

## DEVELOPER INFO

**NAME**: Stas Kasabov

**E-MAIL**: stas@kasabov.ru

## SOFTWARE

**OS**: Windows 10 Pro 21H2

**JDK**: OPENJDK 1.8.0_322

## HARDWARE

**CPU**: i5-9600K

**RAM**: 16GB

**SSD**: 512GB

## RUN PROGRAM

```shell
java -jar task-manager.jar
```
